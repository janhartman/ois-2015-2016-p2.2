
     ,-----.,--.                  ,--. ,---.   ,--.,------.  ,------.
    '  .--./|  | ,---. ,--.,--. ,-|  || o   \  |  ||  .-.  \ |  .---'
    |  |    |  || .-. ||  ||  |' .-. |`..'  |  |  ||  |  \  :|  `--, 
    '  '--'\|  |' '-' ''  ''  '\ `-' | .'  /   |  ||  '--'  /|  `---.
     `-----'`--' `---'  `----'  `---'  `--'    `--'`-------' `------'
    ----------------------------------------------------------------- 


Hi there! Welcome to Cloud9 IDE!

To get you started, create some files, play with the terminal,
or visit http://docs.c9.io for our documentation.
If you want, you can also go watch some training videos at
http://www.youtube.com/user/c9ide.

Happy coding!
The Cloud9 IDE team

Node.js spletna klepetalnica

4. vaje pri predmetu Osnove informacijskih sistemov (navodila)


Vzpostavitev okolja

Na BitBucket je na voljo javni repozitorij https://bitbucket.org/dlavbic/ois-2015-2016-p2.2, ki vsebuje Node.js spletno klepetalnico predstavljeno na predavanjih. Z uporabo funkcije Fork ustvarite lastno kopijo repozitorija v okviru katere boste opravljali vaje. V razvojnem okolju Cloud9 ustvarite lokalni repozitorij in ga povežite z oddaljenim BitBucket repozitorijem. V okviru vaj popravite in dopolnite obstoječo implementacijo spletne klepetalnice kot zahtevajo navodila. Med delom smiselno uveljavljajte spremembe v lokalnem in oddaljenem repozitoriju!

Stilska preobrazba klepetalnice

Trenutni izgled spletne klepetalnice je prilagojen na širino 800
točk. Slednje popravite tako, da bo širina spletne klepetalnice prilagojena širini okna brskalnika. Širina področja za sporočila naj predstavlja 85% širine okna brskalnika, dočim naj seznam kanalov predstavlja 15%

širine okna brskalnika. Popravite tudi barvno lestvico klepetalnice tako, da barvo ozadja sporočil nastavite na svetlejši odtenek rumene #FFFFCC ter barvo ozadja naziva kanala na nekoliko temnejši odtenek rumene #FFFF99. (Spremembo implementirajte v CSS stilski datoteki public/css/stil.css.)

Prikaz vzdevka uporabnika

V trenutni verziji spletne klepetalnice se nad sporočili na kanalu nahaja podatek o trenutno aktivnem kanalu (npr. Skedenj). Popravite implementacijo spletne klepetalnice tako, da bo omenjeno besedilo vključevalo tudi podatek o aktualnem vzdevku uporabnika. Izpis naj bo oblike Dejan @ Skedenj, kjer je Dejan aktualni vzdevek uporabnika in Skedenj trenutno aktiven kanal. (Spremembo implementirajte v JavaScript datoteki public/js/klepet_UI.js.)

Filter vulgarnih besed

Implementirajte filter vulgarnih angleških besed, kjer si pomagajte s seznamom besed na spletni strani http://www.bannedwordlist.com. Uporabniku naj bo dovoljen vnos vulgarnih besed, dočim pa naj se filter aktivira ob prikazu sporočila na kanalu. Natančneje, ob pojavitvi vulgarne besede iz omenjenega seznama, naj se vsaka črka besede nadomesti z znakom *. Na primer, pri vnosu sporočila Lovro is a jerk. naj se na kanalu izpiše Lovro is a ****.. Filtriranje naj deluje zgolj na celotnih besedah; če se vulgarna beseda pojavi kot podniz druge besede, potem filtra ne aktivirajte.

Funkcionalnost naj temelji na seznamu vulgarnih besed v datoteki swearWords.txt, swearWords.xml ali swearWords.csv, ki jo najdete na na spletni strani http://www.bannedwordlist.com in jo shranite v svoj repozitorij v mapo public. Do vsebine datoteke dostopajte preko predpomnilnika, kot je to implementirano v okviru strežbe statičnih spletnih strani.(Spremembo implementirajte v JavaScript datoteki public/js/klepet_UI.js.)